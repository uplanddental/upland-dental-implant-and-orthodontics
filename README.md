We are a multi specialty dental group practice in Upland, Rancho Cucamonga, Chino and Wildomar. We have family dentists, orthodontists, endodontist, periodontist, dental implant specialist, oral surgery and dental anesthesiologist.

Address: 11328 Kenyon Way, Rancho Cucamonga, CA 91701, USA

Phone: 909-945-5800

Website: http://www.myuplanddental.com
